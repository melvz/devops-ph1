
## This is the actual working Terraform script that simply creates two nodes, with the label names "pepsi" incremented by pepsi(n+1).
##

variable "region" {
  default = "ap-southeast-1"
}
variable "shared_credentials_file" {
  default = "/Users/raymundmelvinchua/.aws/credentials"
}

variable "profile" {
  default = "infra"
}

variable "count" {
  default=1
}


provider "aws" {
  region                  = "${var.region}"
  shared_credentials_file = "${var.shared_credentials_file}"
  profile                 = "${var.profile}"
}
resource "aws_instance" "web" {
  count="${var.count}"
  ami = "ami-0c5199d385b432989"
  instance_type = "t2.micro"
  key_name = "proximax-io-main-site"
  vpc_security_group_ids = ["sg-0ccd2647d77c02822"]
  subnet_id = "subnet-077d1737bb0ec484f"
  associate_public_ip_address = "true"
  tags { Name = "${format("nginx-faucet-%01d",count.index+1)}" }

  user_data = <<-EOF
          #! /bin/bash
          sudo apt update -y
          EOF


  root_block_device {
    volume_type           = "standard"
    volume_size           = "23"
    delete_on_termination = "false"
  }
}
